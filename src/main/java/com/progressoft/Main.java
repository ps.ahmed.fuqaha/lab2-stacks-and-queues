package com.progressoft;

import com.progressoft.stacks.FixedStack;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class Main {
    public static void main(String[] args) {
        FixedStack ds = new FixedStack(3);
        //DynamicQueue ds = new DynamicQueue();
        //DynamicStack ds = new DynamicStack();
        //FixedQueue ds = new FixedQueue(4);
        ds.push(1);
        ds.push(2);
        ds.push(3);
        //ds.push(4);
        //System.out.println(ds.peek());
        System.out.println(ds.pop());
        System.out.println(ds.pop());
        System.out.println(ds.pop());
        System.out.println(ds.size());
    }
}