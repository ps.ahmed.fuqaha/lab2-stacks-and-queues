package com.progressoft.stacks;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class CustomStackIterator<T> implements Iterator<T> {

    private int currentIndex;
    private T[] array;

    CustomStackIterator(T[] array) {
        this.array = array;
        currentIndex = array.length - 1;
    }
    @Override
    public boolean hasNext() {
        return currentIndex != -1;
    }

    @Override
    public T next() {
        if (currentIndex == -1)
            throw new NoSuchElementException("no such element");
        return array[currentIndex--];
    }
}
