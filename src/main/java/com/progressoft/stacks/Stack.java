package com.progressoft.stacks;

import java.util.Arrays;

public abstract class Stack {

    private int array[];
    private int lastElement;
    private int capacity = 3;

    public Stack() {
        array = new int[capacity];
        lastElement = -1;
    }

    public Stack(int capacity) {
        array = new int[capacity];
        lastElement = -1;
    }

    public void push(int element) {
        if (isFull()) {
            array = handleFull(Arrays.copyOf(array, array.length));
            capacity = array.length;
        }
        array[lastElement + 1] = element;
        lastElement++;
    }

    // TODO template method design pattern
    // TODO Liskov principle
    protected abstract int[] handleFull(int[] values);

    public int pop() {
        int v = peek();
        lastElement--;
        return v;
    }

    public int peek() {
        if (!isEmpty())
            return array[lastElement];
        throw new IllegalStateException("Stack is empty ");
    }

    private boolean isEmpty() {
        return lastElement == -1;
    }

    public boolean isFull() {
        return lastElement == capacity - 1;
    }

    public int size() {
        return lastElement + 1;
    }
}
