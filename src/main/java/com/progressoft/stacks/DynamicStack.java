package com.progressoft.stacks;

public class DynamicStack extends Stack {

    public DynamicStack() {
        super();
    }

    @Override
    protected int[] handleFull(int[] values) {
        return resize(values);
    }

    public int[] resize(int[] values) {
        int[] biggerArray = new int[values.length * 2];
        for (int i = 0; i < values.length; i++)
            biggerArray[i] = values[i];
        return biggerArray;
    }
}
