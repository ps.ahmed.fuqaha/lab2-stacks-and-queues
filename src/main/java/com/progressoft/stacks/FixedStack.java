package com.progressoft.stacks;

public class FixedStack extends Stack {

    public FixedStack(int capacity) {
        super(capacity);
    }

    @Override
    protected int[] handleFull(int[] values) {
        throw new IllegalStateException("element cannot be added as the stack is full");
    }


}

