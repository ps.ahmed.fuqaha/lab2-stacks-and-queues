package com.progressoft.stacks;

import java.util.Arrays;
import java.util.Iterator;

public class GenericStack<T> implements Iterable<T> {

    private T array[];
    private int lastElement;
    private int capacity;

    public GenericStack(int capacity) {
        array = (T[]) new Object[capacity];
        lastElement = -1;
        this.capacity=capacity;
    }

    public void push(T element) {
        if (isFull())
            throw new IllegalStateException("Stack is full");

        array[lastElement + 1] = element;
        lastElement++;
    }

    public T pop() {
        if (isEmpty())
            throw new IllegalStateException("stack is empty");
        T v = peek();
        lastElement--;
        return v;
    }

    public T peek() {
        if (!isEmpty())
            return array[lastElement];
        throw new IllegalStateException("Stack is empty ");
    }

    private boolean isEmpty() {
        return lastElement == -1;
    }

    public boolean isFull() {
        return lastElement == capacity - 1;
    }

    public int size() {
        return lastElement + 1;
    }

    @Override
    public Iterator<T> iterator() {
        return new CustomStackIterator<T>(array);
    }
}
