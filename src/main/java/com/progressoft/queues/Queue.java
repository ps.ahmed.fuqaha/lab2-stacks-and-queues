package com.progressoft.queues;

import java.util.Arrays;

public abstract class Queue {
    // TODO do the same notes as done in the stack

    private int[] array;
    private int lastElement;
    private int capacity = 3;

    public Queue() {
        array = new int[capacity];
        lastElement = -1;
    }

    public Queue(int capacity) {
        array = new int[capacity];
        lastElement = -1;
    }

    protected void enque(int element) {
        if (isFull()) {
            array = handleFull(Arrays.copyOf(array, array.length));
            capacity = array.length;
        }
        shift();
        array[0] = element;
        lastElement++;
    }

    public int deque() {
        int value = peek();
        lastElement--;
        return value;
    }

    public int peek() {
        if (!isEmpty())
            return array[lastElement];
        throw new IllegalStateException("Stack is empty ");
    }

    private void shift() {
        for (int i = array.length - 1; i > 0; i--)
            array[i] = array[i - 1];
    }

    protected abstract int[] handleFull(int[] array);

    private boolean isEmpty() {
        return lastElement == -1;
    }

    public boolean isFull() {
        return lastElement == capacity - 1;
    }

    public int size() {
        return lastElement + 1;
    }
}
