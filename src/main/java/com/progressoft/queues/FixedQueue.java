package com.progressoft.queues;

public class FixedQueue extends Queue {

    public FixedQueue(int capacity){
        super(capacity);
    }
    @Override
    protected int[] handleFull(int[] array) {
        throw new IllegalStateException("element cannot be added as the queue is full");
    }
}
