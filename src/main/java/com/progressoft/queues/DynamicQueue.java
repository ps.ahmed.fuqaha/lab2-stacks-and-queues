package com.progressoft.queues;

public class DynamicQueue extends Queue {

    public DynamicQueue() {
        super();
    }

    @Override
    protected int[] handleFull(int[] temporary) {
        return expand(temporary);
    }

    private int[] expand(int [] temporary) {
        int[] biggerArray = new int[temporary.length * 2];
        for (int i = 0; i < temporary.length; i++)
            biggerArray[i] = temporary[i];
        return biggerArray;
    }
}
