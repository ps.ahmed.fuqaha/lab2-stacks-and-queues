package com.progressoft;

import com.progressoft.stacks.DynamicStack;
import com.progressoft.stacks.FixedStack;
import com.progressoft.stacks.GenericStack;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class GenericStackTest {

    @Test
    public void givenEmptyStack_whenPop_thenThrowException() {
        GenericStack<Integer> genericStack = new GenericStack<>(5);
        IllegalStateException thrown = Assertions.assertThrows(IllegalStateException.class, () -> genericStack.pop());
        Assertions.assertSame("stack is empty", thrown.getMessage());

    }

    @Test
    public void givenStack_whenPopAndPush_thenValidateOperations() {
        GenericStack<Integer> genericStack = new GenericStack<>(3);
        genericStack.push(1);
        genericStack.push(2);
        genericStack.push(3);
        IllegalStateException thrown = Assertions.assertThrows(IllegalStateException.class, () -> genericStack.push(3));
        Assertions.assertSame("Stack is full", thrown.getMessage());
        Assertions.assertSame(3,genericStack.pop());
        Assertions.assertSame(2,genericStack.pop());
        Assertions.assertSame(1,genericStack.pop());
    }

    @Test
    public void givenStack_whenIterateOverElements_thenSuccess() {
        GenericStack<String> genericStack = new GenericStack<>(3);
        genericStack.push("A");
        genericStack.push("B");
        genericStack.push("C");
        Iterator<String> iterator = genericStack.iterator();
        Assertions.assertNotNull(iterator, "iterator is null");

        Assertions.assertTrue(iterator.hasNext(), "should return true for C");
        Assertions.assertSame("C", iterator.next());

        Assertions.assertTrue(iterator.hasNext(), "should return true for B");
        Assertions.assertSame("B", iterator.next());

        Assertions.assertTrue(iterator.hasNext(), "should return true for A");
        Assertions.assertSame("A", iterator.next());

        Assertions.assertFalse(iterator.hasNext());
        Assertions.assertThrows(NoSuchElementException.class, () -> iterator.next());


    }


}
